'use strict';

/* jshint globalstrict: true */
/* global dc,d3,crossfilter,colorbrewer */

var q = queue()
    .defer(d3.csv, "/chats");

q.await(function(error, data) {
// columns are
// date length text chat_id time
    // var hourFormat = d3.time.format("%X");
    var dateFormat = d3.time.format("%Y-%m-%d"),
        timeFormat = d3.time.format("%H:%M:%S"),
        hourFormat = d3.time.format("%H"),
        monthShortFormat = d3.time.format("%b"), //abbreviated month name.
        dayNameFormat = d3.time.format("%A"),
        shortFormat = d3.format('02d');


    // charts
     var txtLengthChart = dc.barChart("#txtLengthChart"),
        txtCountChart = dc.barChart('#txtCountChart'),
        txtTable = dc.dataTable("#dc-data-table"),
        piePersonSource = dc.pieChart("#piePersonSource"),
        meOrThemChart = dc.pieChart("#meOrThemChart"),
        byMonth = dc.rowChart("#byMonth"),
        totalTextsND = dc.numberDisplay("#totalTextsND");

    // parse data from csv
    data.forEach(function(d) {
        d.date = dateFormat.parse(d.date);
        d.time = timeFormat.parse(d.time);
        d.txtlength = +d.txtlength;
        d.text = d.text;
        d.chatid = d.chatid;
        d.userid = d.userid.slice(0,5);
        d.isFromMe = d.is_from_me;
});
    // dimensions


     var ndx = crossfilter(data),
        all = ndx.groupAll(),
        dateDimension = ndx.dimension(function(d) { return d3.time.day(d.date); }),
        dates = dateDimension.group(),
        hourDimension = ndx.dimension(function(d) { return d.time.getHours() + d.time.getMinutes() / 60; }),
        hours = hourDimension.group(Math.floor),
        lengthHourlyGroupSum = hourDimension.group().reduceSum(function (d) { return d.txtlength;}),
        lengthHourlyGroupCount = hourDimension.group().reduceCount(function (d) { return d.text;}),
        sourceDimension = ndx.dimension(function(d) { return d.userid;}),
        sourceGrouped = sourceDimension.group(),
        meOrThemDimension = ndx.dimension(function (d) {if (d.isFromMe==0) return "Them"; else return "Me";}),
        meOrThemGrouped = meOrThemDimension.group(),
        monthDimension = ndx.dimension(function (d) {return monthShortFormat(d.date);}),
        monthGrouped  = monthDimension.group().reduceCount(function(d){return monthShortFormat(d.date);}),
        totalTexts = ndx.groupAll().reduceCount(function(d) {return d.txtlength;});
    // CHARTS
    // totalTexts
    totalTextsND
        .formatNumber(d3.format("d"))
        .valueAccessor(function(d){return d; })
        .group(all);
        // .formatNumber(d3.format(".3s"));

    //source piechart
    piePersonSource
        .width(300)
        .height(250)
        .radius(100)
        .innerRadius(20)
        .dimension(sourceDimension)
        .group(sourceGrouped)
        .cap(10)
        // .renderTitle(false)
        .title(function(d){return d.userid;});

    // meOrThemChart
    meOrThemChart
        .width(300)
        .height(250)
        .radius(100)
        .innerRadius(20)
        .dimension(meOrThemDimension)
        .group(meOrThemGrouped);
        // .title(function(d){return d.;});

    byMonth
        .width(300)
        .height(250)
        .gap(1)
        .ordering(function(d){return d.monthGrouped;})
        .margins({top: 20, left: 10, right: 10, bottom: -1})
        .group(monthGrouped)
        .dimension(monthDimension)
        // .label(function(d){return monthDimension;})
        // .title(function(d){return ' x';})
        .elasticX(true)
        .xAxis()
        .ticks(0);


     // txtLengthChart stuff
     txtLengthChart
        .width(500)
        .height(250)
        .dimension(hourDimension)
        .group(lengthHourlyGroupSum)
        .transitionDuration(500)
        .margins({top: 20, right: 10, bottom: 50, left: -5})
        .elasticX(true)
        .elasticY(true)
        .centerBar(true)
        .x(d3.scale.linear().domain([0, 25]));
        // .filter([3, 5])
        // .xAxis().tickFormat();

     txtCountChart
        .width(500)
        .height(250)
        .dimension(hourDimension)
        .group(lengthHourlyGroupCount)
        .transitionDuration(500)
        .elasticX(true)
        .elasticY(true)
        .margins({top: 20, right: 10, bottom: 50, left: -5})
        .x(d3.scale.linear().domain([0, 25]));

     // txtTable stuff
     txtTable
         .dimension(dateDimension)
         .group(function(d) {
            return shortFormat((d.date.getMonth() + 1)) + '/' + d.date.getDate();})
         .size(20)
         .columns([
             function (d) { return timeFormat(d.time); },
             function (d) { return d.text; },
             function (d) { return d.txtlength; },
             function (d) { return d.chatid; },
             function (d) { return d.userid;}
             ])
         .sortBy(function(d) {return d.day;})
         .order(d3.ascending);

     dc.renderAll();
 });
