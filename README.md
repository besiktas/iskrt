# iMessage visualization made simple

uses ~/Library/Messages/chat.db
and address book contacts

Example:
![in use](vis.gif)

# current charts/metrics

- source or destination (who the text was to or from)
- who sent it (you or them)
- number of texts by month
- amount of selected texts
- length of text by hour
- number of texts by hour
- table displaying texts

![screenshot](screenshot.png)


# Usage

1. Download repo
2. cd into repo
3. pip3 install -r requirements.txt
4. run python manage.py server

# ToDo

- Plan to add sklearn/stats based stuff in another tab
- Other stuff
- Trim down extraneous
