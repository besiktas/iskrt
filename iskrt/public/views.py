# -*- coding: utf-8 -*-
# from iskrt.utils import flash_errors
from flask import Blueprint, flash, render_template
from flask import Response
import os
from os import path
import pandas as pd
import sqlite3


blueprint = Blueprint('public', __name__, static_folder='../static')


def get_abcddb():
    '''abcddb files are the address book files
    '''
    paths = []
    generic_path = path.expanduser(
        # not sure if the .abcddb in just addressbook directory has anything
        "~") + "/Library/Application Support/AddressBook/Sources/"
    for dirpath, dirnames, filenames in os.walk(generic_path):
        for filename in [f for f in filenames if f.endswith(".abcddb")]:
            paths.append(dirpath + '/' + filename)
    return paths


def get_contacts():
    '''return dict of contacts
    '''
    query = '''SELECT
                    ZABCDRECORD.ZFIRSTNAME,
                    ZABCDRECORD.ZLASTNAME,
                    ZABCDPHONENUMBER.ZFULLNUMBER
                        FROM ZABCDRECORD
                    LEFT JOIN ZABCDPHONENUMBER
                        ON ZABCDPHONENUMBER.ZOWNER = ZABCDRECORD.Z_PK;'''
    cdict = {}
    paths = get_abcddb()
    for path in paths:
        print('path is: ', path)
        conn = sqlite3.connect(path)
        contacts = pd.read_sql(query, conn)
        conn.close()
        contacts = contacts.dropna(axis=0)
        for row in contacts.iterrows():
            first_name, last_name, number = row[1][:]
            try:
                # terrible way to standardize phone numbers but seems to work
                number = "".join(number.split())
                for x in ["(", ")", "+1", "+", "-"]:
                    number = number.replace(x, "")
                if number[0] == '1':
                    number.pop(0)
                number = "+1" + number
                cdict[number] = first_name # + ' ' + last_name
            except AttributeError:
                # no number
                pass
    return cdict


def get_texts():
    """
    formatting here versus d3 incase of wish to use sklearn stuff
    """
    query = '''SELECT
                    datetime(date + 978307200, 'unixepoch', 'localtime') as date,
                    handle.id as userid,
                    is_from_me,
                    text,
                    length(text) as txtlength,
                    chat_id as chatid
                    FROM message
                        JOIN handle
                            ON message.handle_id=handle.ROWID
                        JOIN chat_message_join
                            ON message.rowid = chat_message_join.message_id
                        -- ORDER BY date DESC LIMIT 87742;
                        ORDER BY date DESC;
    '''
    conn = sqlite3.connect(path.expanduser("~") + "/Library/Messages/chat.db")
    texts = pd.read_sql(query, conn)
    texts = texts.dropna(axis=0)
    texts['date'] = pd.to_datetime(texts['date'])
    texts['time'], texts['date'] = texts['date'].apply(
        lambda x: x.time()), texts['date'].apply(lambda x: x.date())
    texts['txtlength'] = texts['txtlength'].astype(int)
    cdict = get_contacts()
    texts = texts.replace({"userid": cdict})
    # remove zalgo
    texts['text'] = texts['text'].replace(
        'effective. \rPower\rلُلُصّبُلُلصّبُررً ॣ ॣh ॣ ॣ\r冗', 'effectivepowerzalgo')
    # texts.to_csv('chat.csv', index=False)
    return texts
    # texts = texts[['date', 'length']]


@blueprint.route('/', methods=['GET', 'POST'])
def home():
    """Home page."""
    # flash('using new chat.db', 'success')
    return render_template('public/home.html')


@blueprint.route('/chats')
def chats():
    texts = get_texts()
    # texts = pd.read_csv('/iskrt/static/data/chat.csv')
    # texts.to_csv('chat.csv',encoding='utf-8')
    return Response(texts.to_csv(index=False), mimetype="text/csv")


@blueprint.route('/about/')
def about():
    """About page."""
    return render_template('public/about.html')
